import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:fsel_work_notes/models/work_note_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'work_note_states.g.dart';

enum WorkNoteStatus { init, loading, success, fail }

@JsonSerializable()
@CopyWith()
class WorkNoteStates extends Equatable {
  final List<WorkNoteModel> notes;
  final WorkNoteStatus createWorkNoteStatus;
  final WorkNoteStatus editWorkNoteStatus;
  final WorkNoteStatus deleteWorkNoteStatus;
  final bool isChecked;

  WorkNoteStates({
    this.notes = const [],
    this.createWorkNoteStatus = WorkNoteStatus.init,
    this.editWorkNoteStatus = WorkNoteStatus.init,
    this.deleteWorkNoteStatus = WorkNoteStatus.init,
    this.isChecked = false,
  });

  @override
  List<Object?> get props => [
        notes,
        createWorkNoteStatus,
        editWorkNoteStatus,
        deleteWorkNoteStatus,
        isChecked,
      ];

  factory WorkNoteStates.fromJson(Map<String, dynamic> json) =>
      _$WorkNoteStatesFromJson(json);

  Map<String, dynamic> toJson() => _$WorkNoteStatesToJson(this);
}
