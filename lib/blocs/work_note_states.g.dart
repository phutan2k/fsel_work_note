// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'work_note_states.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$WorkNoteStatesCWProxy {
  WorkNoteStates createWorkNoteStatus(WorkNoteStatus createWorkNoteStatus);

  WorkNoteStates deleteWorkNoteStatus(WorkNoteStatus deleteWorkNoteStatus);

  WorkNoteStates editWorkNoteStatus(WorkNoteStatus editWorkNoteStatus);

  WorkNoteStates isChecked(bool isChecked);

  WorkNoteStates notes(List<WorkNoteModel> notes);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WorkNoteStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WorkNoteStates(...).copyWith(id: 12, name: "My name")
  /// ````
  WorkNoteStates call({
    WorkNoteStatus? createWorkNoteStatus,
    WorkNoteStatus? deleteWorkNoteStatus,
    WorkNoteStatus? editWorkNoteStatus,
    bool? isChecked,
    List<WorkNoteModel>? notes,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfWorkNoteStates.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfWorkNoteStates.copyWith.fieldName(...)`
class _$WorkNoteStatesCWProxyImpl implements _$WorkNoteStatesCWProxy {
  final WorkNoteStates _value;

  const _$WorkNoteStatesCWProxyImpl(this._value);

  @override
  WorkNoteStates createWorkNoteStatus(WorkNoteStatus createWorkNoteStatus) =>
      this(createWorkNoteStatus: createWorkNoteStatus);

  @override
  WorkNoteStates deleteWorkNoteStatus(WorkNoteStatus deleteWorkNoteStatus) =>
      this(deleteWorkNoteStatus: deleteWorkNoteStatus);

  @override
  WorkNoteStates editWorkNoteStatus(WorkNoteStatus editWorkNoteStatus) =>
      this(editWorkNoteStatus: editWorkNoteStatus);

  @override
  WorkNoteStates isChecked(bool isChecked) => this(isChecked: isChecked);

  @override
  WorkNoteStates notes(List<WorkNoteModel> notes) => this(notes: notes);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WorkNoteStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WorkNoteStates(...).copyWith(id: 12, name: "My name")
  /// ````
  WorkNoteStates call({
    Object? createWorkNoteStatus = const $CopyWithPlaceholder(),
    Object? deleteWorkNoteStatus = const $CopyWithPlaceholder(),
    Object? editWorkNoteStatus = const $CopyWithPlaceholder(),
    Object? isChecked = const $CopyWithPlaceholder(),
    Object? notes = const $CopyWithPlaceholder(),
  }) {
    return WorkNoteStates(
      createWorkNoteStatus:
          createWorkNoteStatus == const $CopyWithPlaceholder() ||
                  createWorkNoteStatus == null
              ? _value.createWorkNoteStatus
              // ignore: cast_nullable_to_non_nullable
              : createWorkNoteStatus as WorkNoteStatus,
      deleteWorkNoteStatus:
          deleteWorkNoteStatus == const $CopyWithPlaceholder() ||
                  deleteWorkNoteStatus == null
              ? _value.deleteWorkNoteStatus
              // ignore: cast_nullable_to_non_nullable
              : deleteWorkNoteStatus as WorkNoteStatus,
      editWorkNoteStatus: editWorkNoteStatus == const $CopyWithPlaceholder() ||
              editWorkNoteStatus == null
          ? _value.editWorkNoteStatus
          // ignore: cast_nullable_to_non_nullable
          : editWorkNoteStatus as WorkNoteStatus,
      isChecked: isChecked == const $CopyWithPlaceholder() || isChecked == null
          ? _value.isChecked
          // ignore: cast_nullable_to_non_nullable
          : isChecked as bool,
      notes: notes == const $CopyWithPlaceholder() || notes == null
          ? _value.notes
          // ignore: cast_nullable_to_non_nullable
          : notes as List<WorkNoteModel>,
    );
  }
}

extension $WorkNoteStatesCopyWith on WorkNoteStates {
  /// Returns a callable class that can be used as follows: `instanceOfWorkNoteStates.copyWith(...)` or like so:`instanceOfWorkNoteStates.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$WorkNoteStatesCWProxy get copyWith => _$WorkNoteStatesCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkNoteStates _$WorkNoteStatesFromJson(Map<String, dynamic> json) =>
    WorkNoteStates(
      notes: (json['notes'] as List<dynamic>?)
              ?.map((e) => WorkNoteModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      createWorkNoteStatus: $enumDecodeNullable(
              _$WorkNoteStatusEnumMap, json['createWorkNoteStatus']) ??
          WorkNoteStatus.init,
      editWorkNoteStatus: $enumDecodeNullable(
              _$WorkNoteStatusEnumMap, json['editWorkNoteStatus']) ??
          WorkNoteStatus.init,
      deleteWorkNoteStatus: $enumDecodeNullable(
              _$WorkNoteStatusEnumMap, json['deleteWorkNoteStatus']) ??
          WorkNoteStatus.init,
      isChecked: json['isChecked'] as bool? ?? false,
    );

Map<String, dynamic> _$WorkNoteStatesToJson(WorkNoteStates instance) =>
    <String, dynamic>{
      'notes': instance.notes,
      'createWorkNoteStatus':
          _$WorkNoteStatusEnumMap[instance.createWorkNoteStatus]!,
      'editWorkNoteStatus':
          _$WorkNoteStatusEnumMap[instance.editWorkNoteStatus]!,
      'deleteWorkNoteStatus':
          _$WorkNoteStatusEnumMap[instance.deleteWorkNoteStatus]!,
      'isChecked': instance.isChecked,
    };

const _$WorkNoteStatusEnumMap = {
  WorkNoteStatus.init: 'init',
  WorkNoteStatus.loading: 'loading',
  WorkNoteStatus.success: 'success',
  WorkNoteStatus.fail: 'fail',
};
