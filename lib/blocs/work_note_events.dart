import 'package:equatable/equatable.dart';
import 'package:fsel_work_notes/models/work_note_model.dart';

sealed class WorkNoteEvents extends Equatable {
  WorkNoteEvents();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class CreateWorkNoteEvent extends WorkNoteEvents {
  final WorkNoteModel workNoteModel;

  CreateWorkNoteEvent(this.workNoteModel);

  @override
  List<Object?> get props => [];
}

class EditWorkNoteEvent extends WorkNoteEvents {
  final WorkNoteModel workNoteModel;

  EditWorkNoteEvent(this.workNoteModel);

  @override
  List<Object?> get props => [];
}

class DeleteWorkNoteEvent extends WorkNoteEvents {
  final int id;

  DeleteWorkNoteEvent(this.id);

  @override
  List<Object?> get props => [];
}

class CheckBoxEvent extends WorkNoteEvents {
  final bool isChecked;

  CheckBoxEvent(this.isChecked);

  @override
  List<Object?> get props => [];
}