import 'dart:async';

import 'package:fsel_work_notes/blocs/work_note_events.dart';
import 'package:fsel_work_notes/blocs/work_note_states.dart';
import 'package:fsel_work_notes/models/work_note_model.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

class WorkNoteBloc extends HydratedBloc<WorkNoteEvents, WorkNoteStates> {
  WorkNoteBloc() : super(WorkNoteStates()) {
    on<CreateWorkNoteEvent>(_createWorkNoteEvent);
    on<EditWorkNoteEvent>(_editWorkNoteEvent);
    on<DeleteWorkNoteEvent>(_deleteWorkNoteEvent);
    on<CheckBoxEvent>(_checkBoxEvent);
  }

  @override
  WorkNoteStates? fromJson(Map<String, dynamic> json) {
    return WorkNoteStates.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(WorkNoteStates state) {
    return state.toJson();
  }

  FutureOr<void> _createWorkNoteEvent(
    CreateWorkNoteEvent event,
    Emitter<WorkNoteStates> emit,
  ) {
    emit(state.copyWith(createWorkNoteStatus: WorkNoteStatus.loading));

    final notes = [...state.notes];

    notes.add(WorkNoteModel(
      id: notes.length + 1,
      title: event.workNoteModel.title,
      description: event.workNoteModel.description,
      status: event.workNoteModel.status,
    ));

    emit(state.copyWith(
      notes: notes,
      createWorkNoteStatus: WorkNoteStatus.success,
    ));
  }

  FutureOr<void> _editWorkNoteEvent(
    EditWorkNoteEvent event,
    Emitter<WorkNoteStates> emit,
  ) {
    emit(state.copyWith(editWorkNoteStatus: WorkNoteStatus.loading));

    final notes = [...state.notes];
    final index =
        notes.indexWhere((element) => element.id == event.workNoteModel.id);

    if (index != -1) {
      notes[index] = WorkNoteModel(
        id: event.workNoteModel.id,
        title: event.workNoteModel.title,
        description: event.workNoteModel.description,
        status: event.workNoteModel.status,
      );
    }

    emit(state.copyWith(
      notes: notes,
      editWorkNoteStatus: WorkNoteStatus.success,
    ));
  }

  FutureOr<void> _deleteWorkNoteEvent(
    DeleteWorkNoteEvent event,
    Emitter<WorkNoteStates> emit,
  ) {
    emit(state.copyWith(deleteWorkNoteStatus: WorkNoteStatus.loading));

    final notes = [...state.notes];
    final index = notes.indexWhere((element) => element.id == event.id);

    if (index != -1) {
      notes.remove(notes[index]);
    }

    emit(state.copyWith(
      notes: notes,
      deleteWorkNoteStatus: WorkNoteStatus.success,
    ));
  }

  FutureOr<void> _checkBoxEvent(
    CheckBoxEvent event,
    Emitter<WorkNoteStates> emit,
  ) {
    emit(state.copyWith(isChecked: event.isChecked));
  }
}
