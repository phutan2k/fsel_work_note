import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_work_notes/blocs/work_note_blocs.dart';
import 'package:fsel_work_notes/blocs/work_note_events.dart';
import 'package:fsel_work_notes/blocs/work_note_states.dart';
import 'package:fsel_work_notes/screens/detail_home_page/detail_home_page.dart';
import 'package:fsel_work_notes/widgets/reusable_widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.yellow[200],
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.yellow[200],
            centerTitle: true,
            title: Text('Work Notes'),
          ),
          body: Stack(
            children: [
              Positioned(
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                child: Column(
                  children: [
                    BlocBuilder<WorkNoteBloc, WorkNoteStates>(
                      builder: (context, state) {
                        return Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 10, right: 10, top: 20),
                            child: ListView.builder(
                                itemCount: state.notes.length,
                                itemBuilder: (_, index) {
                                  return GestureDetector(
                                    onTap: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => DetailHomePage(
                                          workNoteModel: state.notes[index],
                                        ),
                                      ),
                                    ),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 80,
                                      margin: EdgeInsets.only(bottom: 20),
                                      padding: EdgeInsets.only(
                                        top: 20,
                                        bottom: 20,
                                        left: 20,
                                        right: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: state.notes[index].status
                                            ? Colors.lightGreen
                                            : Colors.yellow[200],
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                reusableTextWidget(
                                                    text: state
                                                        .notes[index].title,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                reusableTextWidget(
                                                  text: state
                                                      .notes[index].description,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Checkbox(
                                            /**
                                             * TODO: Đang gặp bug chưa xử lý được
                                             */
                                            value: state.notes[index].status,
                                            onChanged: (value) {
                                              BlocProvider.of<WorkNoteBloc>(
                                                      context)
                                                  .add(CheckBoxEvent(value!));
                                            },
                                            activeColor: Colors.orangeAccent,
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 50,
                right: 20,
                child: GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => DetailHomePage())),
                  child: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.yellow[600],
                    ),
                    child: Center(
                      child: reusableTextWidget(
                        text: '+',
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
