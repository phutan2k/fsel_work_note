import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_work_notes/blocs/work_note_blocs.dart';
import 'package:fsel_work_notes/blocs/work_note_events.dart';
import 'package:fsel_work_notes/blocs/work_note_states.dart';
import 'package:fsel_work_notes/models/work_note_model.dart';
import 'package:fsel_work_notes/widgets/detail_home_page_widgets.dart';
import 'package:fsel_work_notes/widgets/reusable_widgets.dart';

class DetailHomePage extends StatefulWidget {
  const DetailHomePage({Key? key, this.workNoteModel}) : super(key: key);
  final WorkNoteModel? workNoteModel;

  @override
  State<DetailHomePage> createState() => _DetailHomePageState();
}

class _DetailHomePageState extends State<DetailHomePage> {
  late TextEditingController titleController;
  late TextEditingController descriptionController;

  @override
  void initState() {
    super.initState();
    titleController =
        TextEditingController(text: widget.workNoteModel?.title ?? '');
    descriptionController =
        TextEditingController(text: widget.workNoteModel?.description ?? '');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.yellow[100],
      child: SafeArea(
        child: Scaffold(
          body: Container(
            color: Colors.yellow[100],
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 65,
                  padding: EdgeInsets.only(right: 15),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: onTapped,
                        child: reusableIconWidget(
                            icon: Icons.arrow_back_ios_new_outlined),
                      ),
                      reusableTextWidget(
                          text: 'Work Note details',
                          size: 20,
                          fontWeight: FontWeight.bold),
                      Expanded(child: SizedBox()),
                      GestureDetector(
                        onTap: () {
                          BlocProvider.of<WorkNoteBloc>(context).add(
                              DeleteWorkNoteEvent(widget.workNoteModel!.id));
                          Navigator.of(context).pop();
                        },
                        child: reusableTextWidget(
                          text: 'Delete',
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Stack(
                    children: [
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0,
                        child: SingleChildScrollView(
                          child: Container(
                            height: MediaQuery.of(context).size.height,
                            color: Colors.yellow[50],
                            padding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BuildTextFieldWidget(
                                  controller: titleController,
                                  hintText: 'Title',
                                  isTitle: true,
                                ),
                                Expanded(
                                  child: BuildTextFieldWidget(
                                    controller: descriptionController,
                                    hintText: 'Description',
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: BlocBuilder<WorkNoteBloc, WorkNoteStates>(
                          builder: (context, state) {
                            return Container(
                              height: 65,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  topRight: Radius.circular(20),
                                ),
                                color: state.isChecked
                                    ? Colors.lightGreen
                                    : Colors.yellow[100],
                              ),
                              child: Center(
                                child: CheckboxListTile(
                                  /**
                                   * TODO:value: widget.workNoteModel?.status ??
                                      state.isChecked,
                                   */
                                  value: state.isChecked,
                                  onChanged: (value) {
                                    BlocProvider.of<WorkNoteBloc>(context)
                                        .add(CheckBoxEvent(value!));
                                  },
                                  activeColor: Colors.orangeAccent,
                                  title: state.isChecked
                                      ? reusableTextWidget(
                                          text: 'Complete',
                                          size: 20,
                                          color: Colors.white,
                                        )
                                      : reusableTextWidget(
                                          text: 'Incomplete',
                                          size: 20,
                                          color: Colors.black,
                                        ),
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTapped() {
    if (titleController.value.text.isEmpty) {
      Navigator.of(context).pop();
    } else {
      if (widget.workNoteModel == null) {
        BlocProvider.of<WorkNoteBloc>(context).add(
          CreateWorkNoteEvent(
            WorkNoteModel(
              id: 0,
              title: titleController.value.text,
              description: descriptionController.value.text,
              status: BlocProvider.of<WorkNoteBloc>(context).state.isChecked,
            ),
          ),
        );
      } else {
        BlocProvider.of<WorkNoteBloc>(context).add(
          EditWorkNoteEvent(
            WorkNoteModel(
              id: widget.workNoteModel!.id,
              title: titleController.value.text,
              description: descriptionController.value.text,
              status: BlocProvider.of<WorkNoteBloc>(context).state.isChecked,
            ),
          ),
        );
      }
      Navigator.of(context).pop();
    }
  }
}
