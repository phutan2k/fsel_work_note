import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'work_note_model.g.dart';

@JsonSerializable()
@CopyWith()
class WorkNoteModel {
  final int id;
  final String title;
  final String description;
  final bool status;

  WorkNoteModel({
    required this.id,
    required this.title,
    this.description = '',
    this.status = false,
  });

  factory WorkNoteModel.fromJson(Map<String, dynamic> json) =>
      _$WorkNoteModelFromJson(json);

  Map<String, dynamic> toJson() => _$WorkNoteModelToJson(this);
}
