// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'work_note_model.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$WorkNoteModelCWProxy {
  WorkNoteModel description(String description);

  WorkNoteModel id(int id);

  WorkNoteModel status(bool status);

  WorkNoteModel title(String title);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WorkNoteModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WorkNoteModel(...).copyWith(id: 12, name: "My name")
  /// ````
  WorkNoteModel call({
    String? description,
    int? id,
    bool? status,
    String? title,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfWorkNoteModel.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfWorkNoteModel.copyWith.fieldName(...)`
class _$WorkNoteModelCWProxyImpl implements _$WorkNoteModelCWProxy {
  final WorkNoteModel _value;

  const _$WorkNoteModelCWProxyImpl(this._value);

  @override
  WorkNoteModel description(String description) =>
      this(description: description);

  @override
  WorkNoteModel id(int id) => this(id: id);

  @override
  WorkNoteModel status(bool status) => this(status: status);

  @override
  WorkNoteModel title(String title) => this(title: title);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WorkNoteModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WorkNoteModel(...).copyWith(id: 12, name: "My name")
  /// ````
  WorkNoteModel call({
    Object? description = const $CopyWithPlaceholder(),
    Object? id = const $CopyWithPlaceholder(),
    Object? status = const $CopyWithPlaceholder(),
    Object? title = const $CopyWithPlaceholder(),
  }) {
    return WorkNoteModel(
      description:
          description == const $CopyWithPlaceholder() || description == null
              ? _value.description
              // ignore: cast_nullable_to_non_nullable
              : description as String,
      id: id == const $CopyWithPlaceholder() || id == null
          ? _value.id
          // ignore: cast_nullable_to_non_nullable
          : id as int,
      status: status == const $CopyWithPlaceholder() || status == null
          ? _value.status
          // ignore: cast_nullable_to_non_nullable
          : status as bool,
      title: title == const $CopyWithPlaceholder() || title == null
          ? _value.title
          // ignore: cast_nullable_to_non_nullable
          : title as String,
    );
  }
}

extension $WorkNoteModelCopyWith on WorkNoteModel {
  /// Returns a callable class that can be used as follows: `instanceOfWorkNoteModel.copyWith(...)` or like so:`instanceOfWorkNoteModel.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$WorkNoteModelCWProxy get copyWith => _$WorkNoteModelCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkNoteModel _$WorkNoteModelFromJson(Map<String, dynamic> json) =>
    WorkNoteModel(
      id: json['id'] as int,
      title: json['title'] as String,
      description: json['description'] as String? ?? '',
      status: json['status'] as bool? ?? false,
    );

Map<String, dynamic> _$WorkNoteModelToJson(WorkNoteModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'status': instance.status,
    };
