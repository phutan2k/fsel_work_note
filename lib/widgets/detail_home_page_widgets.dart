import 'package:flutter/material.dart';

class BuildTextFieldWidget extends StatelessWidget {
  final String hintText;
  final bool isTitle;
  final TextEditingController controller;

  const BuildTextFieldWidget({
    Key? key,
    required this.controller,
    required this.hintText,
    this.isTitle = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: Colors.transparent),
      ),
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        decoration: InputDecoration(
          hintText: hintText,
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
          ),
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
        ),
        style: TextStyle(
          color: Colors.black,
          fontWeight: isTitle ? FontWeight.bold : FontWeight.w400,
          fontSize: isTitle ? 18 : 14,
        ),
      ),
    );
  }
}
